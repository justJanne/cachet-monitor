package main

import (
	"os"
	"strconv"
)

func EnvString(key string, defValue string) string {
	data, valid := os.LookupEnv(key)
	if valid {
		return data
	}
	return defValue
}

func EnvFloat(key string, defValue float64) float64 {
	data, err := strconv.ParseFloat(EnvString(key, ""), 64)
	if err != nil {
		return defValue
	}
	return data
}

func EnvInt(key string, defValue int64) int64 {
	data, err := strconv.ParseInt(EnvString(key, ""), 10, 64)
	if err != nil {
		return defValue
	}
	return data
}

func EnvUInt(key string, defValue uint64) uint64 {
	data, err := strconv.ParseUint(EnvString(key, ""), 10, 64)
	if err != nil {
		return defValue
	}
	return data
}

func EnvBool(key string, defValue bool) bool {
	data, err := strconv.ParseBool(EnvString(key, ""))
	if err != nil {
		return defValue
	}
	return data
}
