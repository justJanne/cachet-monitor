package main

import "time"

type Storage struct {
	datapoints map[int][]Datapoint
}

func NewStorage() Storage {
	return Storage{
		datapoints: make(map[int][]Datapoint),
	}
}

func (s Storage) Store(id int, datapoint Datapoint, duration time.Duration) []Datapoint {
	// Find time of first datapoint we want to keep
	now := time.Now()
	timeThreshold := now.Add(-duration)

	// Append datapoint
	data := append(s.datapoints[id], datapoint)
	// Find first datapoint that should be kept
	minIdx := 0
	for idx, elem := range data {
		if elem.Time.After(timeThreshold) {
			minIdx = idx
			break
		}
	}
	// Remove unwanted datapoints
	data = data[minIdx:]
	// Store datapoints
	s.datapoints[id] = data
	return data
}
