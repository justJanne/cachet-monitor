package main

import "fmt"

type ApiMetric struct {
	Id              int    `json:"id,omitempty"`
	Name            string `json:"name,omitempty"`
	Suffix          string `json:"suffix,omitempty"`
	Description     string `json:"description,omitempty"`
	DefaultValue    string `json:"default_value,omitempty"`
	DisplayChart    int    `json:"display_chart,omitempty"`
	CalcType        int    `json:"calc_type,omitempty"`
	CreatedAt       string `json:"created_at,omitempty"`
	UpdatedAt       string `json:"updated_at,omitempty"`
	DefaultViewName string `json:"default_view_name,omitempty"`
}

type ApiMetricBody struct {
	Name         string `json:"name,omitempty"`
	Suffix       string `json:"suffix,omitempty"`
	Description  string `json:"description,omitempty"`
	DefaultValue string `json:"default_value,omitempty"`
	DisplayChart int    `json:"display_chart,omitempty"`
}

type ApiMetricRepo struct {
	ConfigApi
}

func (api ConfigApi) Metrics() ApiMetricRepo {
	return ApiMetricRepo{
		api,
	}
}

func (repo ApiMetricRepo) Get(id int) (metric ApiMetric, err error) {
	_, err = repo.Request(
		&metric,
		"GET",
		fmt.Sprintf("/metrics/%d", id),
		nil,
	)
	return
}

func (repo ApiMetricRepo) Create(body ApiMetricBody) (metric ApiMetric, err error) {
	_, err = repo.Request(
		&metric,
		"POST",
		"/metrics",
		body,
	)
	return
}

func (repo ApiMetricRepo) Delete(id int) (err error) {
	_, err = repo.Request(
		nil,
		"DELETE",
		fmt.Sprintf("/metrics/%d", id),
		nil,
	)
	return
}
