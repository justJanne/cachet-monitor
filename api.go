package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

type CachetResponse struct {
	Meta MetaResponse    `json:"meta,omitempty"`
	Data json.RawMessage `json:"data"`
}

type MetaResponse struct {
}

func (api ConfigApi) Request(target interface{}, method string, url string, body interface{}) (MetaResponse, error) {
	var err error
	var encodedBody []byte
	if body != nil {
		encodedBody, err = json.Marshal(body)
		if err != nil {
			return MetaResponse{}, err
		}
	}

	targetUri := api.Url + url
	req, err := http.NewRequest(method, targetUri, bytes.NewBuffer(encodedBody))
	if err != nil {
		return MetaResponse{}, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Cachet-Token", api.Token)

	transport := http.DefaultTransport.(*http.Transport)
	transport.TLSClientConfig = &tls.Config{InsecureSkipVerify: api.Insecure}
	client := &http.Client{
		Transport: transport,
	}

	res, err := client.Do(req)
	if err != nil {
		return MetaResponse{}, err
	}

	var data CachetResponse
	err = json.NewDecoder(res.Body).Decode(&data)

	defer req.Body.Close()
	if err != nil {
		return MetaResponse{}, err
	}

	if res.StatusCode != 200 {
		return MetaResponse{}, errors.New(fmt.Sprintf("API Responded with status code %d: %s %s", res.StatusCode, res.Request.Method, res.Request.URL))
	}

	if target != nil {
		err = json.Unmarshal(data.Data, &target)
		if err != nil {
			return MetaResponse{}, err
		}
	}

	return data.Meta, nil
}
