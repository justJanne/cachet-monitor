package main

import (
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)

type QuasselMonitor struct {
	config ConfigComponent
}

func NewQuasselMonitor(config ConfigComponent) *QuasselMonitor {
	return &QuasselMonitor{
		config: config,
	}
}

func (m *QuasselMonitor) Config() *ConfigComponent {
	return &m.config
}

func (m *QuasselMonitor) Measure() (Datapoint, error) {
	var err error
	defer func() {
		if r := recover(); r != nil {
			err = errors.New(fmt.Sprintf("Caught error: %v", r))
		}
	}()

	before := time.Now()
	conn := Connect(ConnectOptions{
		ConnectionOptions: ConnectionOptions{
			Address: m.config.Quassel.Hostname,
			Port:    m.config.Quassel.Port,
			Timeout: time.Duration(m.config.LatencyThresholds.Down) * time.Millisecond,
		},
		Verify: !m.config.Quassel.Insecure,
	})
	defer conn.Close()

	after := time.Now()
	latency := int(after.Sub(before).Nanoseconds() / 1000000)
	if err == nil {
		err = conn.Error()
	}

	if err != nil {
		logrus.Infof("Monitor %s: %s", err)
		return Datapoint{
			Time:    after,
			Up:      false,
			Latency: latency,
		}, nil
	}

	return Datapoint{
		Time:    after,
		Up:      true,
		Latency: latency,
	}, nil
}
