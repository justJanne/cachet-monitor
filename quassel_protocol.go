package main

import (
	"encoding/binary"
)

const ProtocolMagic uint32 = 0x42b33f00

const ProtocolFeatureTls uint8 = 0x01
const ProtocolFeatureCompression uint8 = 0x02

const ProtocolDatastream uint32 = 0x02

type ProtocolInfo struct {
	FlagTLS         bool
	FlagCompression bool
	Data            uint16
	Version         uint8
}

func parseProtocolInfo(data []byte) ProtocolInfo {
	rawFeatures := data[0]
	rawData := data[1:3]
	rawVersion := data[3]

	return ProtocolInfo{
		rawFeatures&ProtocolFeatureTls != 0,
		rawFeatures&ProtocolFeatureCompression != 0,
		binary.BigEndian.Uint16(rawData),
		rawVersion,
	}
}

type ConnectOptions struct {
	ConnectionOptions
	Verify bool
}

func Connect(options ConnectOptions) Connection {
	conn := MakeConnection(options.ConnectionOptions)
	conn.Write(ProtocolMagic | uint32(ProtocolFeatureTls))
	supportedProtocols := []uint32{
		ProtocolDatastream,
	}
	for _, protocol := range supportedProtocols {
		conn.Write(protocol)
	}
	conn.Write(1 << 31)
	conn.Flush()
	conn.protocolInfo = parseProtocolInfo(conn.Read(4))
	if conn.protocolInfo.FlagTLS {
		conn.WithTLS(options.Verify)
	}
	return conn
}
