package main

import "fmt"

type ApiMetricPoint struct {
	Id        int    `json:"id,omitempty"`
	MetricId  int    `json:"metric_id,omitempty"`
	Value     int    `json:"value,omitempty"`
	CreatedAt string `json:"created_at,omitempty"`
	UpdatedAt string `json:"updated_at,omitempty"`
}

type ApiMetricPointBody struct {
	Metric    int    `json:"metric,omitempty"`
	Value     int    `json:"value,omitempty"`
	Timestamp string `json:"timestamp,omitempty"`
}

type ApiMetricPointRepo struct {
	ConfigApi
	MetricId int
}

func (api ConfigApi) MetricPoints(metricId int) ApiMetricPointRepo {
	return ApiMetricPointRepo{
		api,
		metricId,
	}
}

func (repo ApiMetricPointRepo) List() (points []ApiMetricPoint, err error) {
	_, err = repo.Request(
		&points,
		"GET",
		fmt.Sprintf("/metrics/%d/points", repo.MetricId),
		nil,
	)
	return
}

func (repo ApiMetricPointRepo) Create(body ApiMetricPointBody) (point ApiMetricPoint, err error) {
	_, err = repo.Request(
		&point,
		"POST",
		fmt.Sprintf("/metrics/%d/points", repo.MetricId),
		body,
	)
	return
}

func (repo ApiMetricPointRepo) Delete(id int) (err error) {
	_, err = repo.Request(
		nil,
		"DELETE",
		fmt.Sprintf("/metrics/%d/points/%d", repo.MetricId, id),
		nil,
	)
	return
}
