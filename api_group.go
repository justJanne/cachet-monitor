package main

import "fmt"

type ApiGroup struct {
	Id        int    `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	CreatedAt string `json:"created_at,omitempty"`
	UpdatedAt string `json:"updated_at,omitempty"`
	Order     int    `json:"order,omitempty"`
	Collapsed int    `json:"collapsed,omitempty"`
}

type ApiGroupBody struct {
	Name      string `json:"name,omitempty"`
	Order     int    `json:"order,omitempty"`
	Collapsed int    `json:"collapsed,omitempty"`
}

type ApiGroupRepo struct {
	ConfigApi
}

func (api ConfigApi) Groups() ApiGroupRepo {
	return ApiGroupRepo{
		api,
	}
}

func (repo ApiGroupRepo) Get(id int) (group ApiGroup, err error) {
	_, err = repo.Request(
		&group,
		"GET",
		fmt.Sprintf("/components/groups/%d", id),
		nil,
	)
	return
}

func (repo ApiGroupRepo) Create(body ApiGroupBody) (group ApiGroup, err error) {
	_, err = repo.Request(
		&group,
		"POST",
		"/components/groups",
		body,
	)
	return
}

func (repo ApiGroupRepo) Save(id int, body ApiGroupBody) (group ApiGroup, err error) {
	_, err = repo.Request(
		&group,
		"PUT",
		fmt.Sprintf("/components/groups/%d", id),
		body,
	)
	return
}

func (repo ApiGroupRepo) Delete(id int) (err error) {
	_, err = repo.Request(
		nil,
		"DELETE",
		fmt.Sprintf("/components/groups/%d", id),
		nil,
	)
	return
}
