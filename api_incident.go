package main

import "fmt"

type ApiIncident struct {
	Id          int    `json:"id,omitempty"`
	ComponentId int    `json:"component_id,omitempty"`
	Name        string `json:"name,omitempty"`
	Status      int    `json:"status,omitempty"`
	Visible     int    `json:"visible,omitempty"`
	Message     string `json:"message,omitempty"`
	ScheduledAt string `json:"scheduled_at,omitempty"`
	CreatedAt   string `json:"created_at,omitempty"`
	UpdatedAt   string `json:"updated_at,omitempty"`
	DeletedAt   string `json:"deleted_at,omitempty"`
	HumanStatus string `json:"human_status,omitempty"`
}

type ApiIncidentBody struct {
	Name            string `json:"name,omitempty"`
	Message         string `json:"message,omitempty"`
	Status          int    `json:"status,omitempty"`
	Visible         int    `json:"visible,omitempty"`
	ComponentId     int    `json:"component_id,omitempty"`
	ComponentStatus int    `json:"component_status,omitempty"`
	Notify          bool   `json:"notify,omitempty"`
	ApiIncidentCreate
}

type ApiIncidentCreate struct {
	CreatedAt string   `json:"created_at,omitempty"`
	Template  string   `json:"template,omitempty"`
	Vars      []string `json:"vars,omitempty"`
}

type ApiIncidentRepo struct {
	ConfigApi
}

func (api ConfigApi) Incidents() ApiIncidentRepo {
	return ApiIncidentRepo{
		api,
	}
}

func (repo ApiIncidentRepo) Get(id int) (incident ApiIncident, err error) {
	_, err = repo.Request(
		&incident,
		"GET",
		fmt.Sprintf("/incidents/%d", id),
		nil,
	)
	return
}

func (repo ApiIncidentRepo) Create(body ApiIncidentBody) (incident ApiIncident, err error) {
	_, err = repo.Request(
		&incident,
		"POST",
		"/incidents",
		body,
	)
	return
}

func (repo ApiIncidentRepo) Save(id int, body ApiIncidentBody) (incident ApiIncident, err error) {
	_, err = repo.Request(
		&incident,
		"PUT",
		fmt.Sprintf("/incidents/%d", id),
		body,
	)
	return
}

func (repo ApiIncidentRepo) Delete(id int) (err error) {
	_, err = repo.Request(
		nil,
		"DELETE",
		fmt.Sprintf("/incidents/%d", id),
		nil,
	)
	return
}
