package main

type Config struct {
	Api        ConfigApi         `yaml:"api"`
	Immediate  bool              `yaml:"immediate"`
	Components []ConfigComponent `yaml:"components"`
}

type ConfigApi struct {
	Url      string `yaml:"url"`
	Token    string `yaml:"token"`
	Insecure bool   `yaml:"insecure,omitempty"`
}

type ConfigComponent struct {
	Name              string `yaml:"name"`
	ComponentId       int    `yaml:"component,omitempty"`
	MetricId          int    `yaml:"metric,omitempty"`
	Interval          string `yaml:"interval"`
	LatencyThresholds struct {
		Slow int `yaml:"slow,omitempty"`
		Down int `yaml:"down,omitempty"`
	} `yaml:"latency_thresholds,omitempty"`
	OutageThresholds struct {
		Performance int `yaml:"performance,omitempty"`
		Partial     int `yaml:"partial,omitempty"`
		Major       int `yaml:"major,omitempty"`
	} `yaml:"outage_thresholds,omitempty"`
	ThresholdDuration string `yaml:"threshold_duration"`
	Type              string `yaml:"type"`
	Http              struct {
		Url      string            `yaml:"url,omitempty"`
		Method   string            `yaml:"method,omitempty"`
		Insecure bool              `yaml:"insecure,omitempty"`
		Headers  map[string]string `yaml:"headers,omitempty"`
		Expected struct {
			StatusCode int    `yaml:"status_code,omitempty"`
			Body       string `yaml:"body,omitempty"`
		} `yaml:"expected,omitempty"`
	} `yaml:"http,omitempty"`
	Quassel struct {
		Hostname string `yaml:"hostname,omitempty"`
		Port     int    `yaml:"port,omitempty"`
		Insecure bool   `yaml:"insecure,omitempty"`
	} `yaml:"quassel,omitempty"`
	Postgres struct {
		Hostname string `yaml:"hostname,omitempty"`
		Port     int    `yaml:"port,omitempty"`
		Username string `yaml:"username,omitempty"`
		Password string `yaml:"password,omitempty"`
		Database string `yaml:"database,omitempty"`
		Insecure bool   `yaml:"insecure,omitempty"`
	} `yaml:"postgres,omitempty"`
}
