package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"time"
)

type PostgresMonitor struct {
	config ConfigComponent
}

func NewPostgresMonitor(config ConfigComponent) *PostgresMonitor {
	return &PostgresMonitor{
		config: config,
	}
}

func (m *PostgresMonitor) Config() *ConfigComponent {
	return &m.config
}

func (m *PostgresMonitor) Measure() (Datapoint, error) {
	sslMode := "verify-full"
	if m.config.Postgres.Insecure {
		sslMode = "allow"
	}
	connStr := fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=%s",
		m.config.Postgres.Username,
		m.config.Postgres.Password,
		m.config.Postgres.Hostname,
		m.config.Postgres.Port,
		m.config.Postgres.Database,
		sslMode,
	)
	before := time.Now()
	db, err := sql.Open("postgres", connStr)
	after := time.Now()
	latency := int(after.Sub(before).Nanoseconds() / 1000000)
	defer db.Close()
	if err != nil {
		logrus.Infof("Monitor %s: %s", err)
		return Datapoint{
			Time:    after,
			Up:      false,
			Latency: latency,
		}, nil
	}

	return Datapoint{
		Time:    after,
		Up:      true,
		Latency: latency,
	}, nil
}
