package main

import (
	"flag"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"os/signal"
	"sync"
)

func setDefaultInt(target *int, defValue int) {
	if *target == 0 {
		*target = defValue
	}
}

func setDefaultString(target *string, defValue string) {
	if *target == "" {
		*target = defValue
	}
}

func main() {
	configPath := flag.String("config", "config.yaml", "Path to config file")
	flag.Parse()

	var config Config
	file, err := ioutil.ReadFile(*configPath)
	if err != nil {
		logrus.Error(err.Error())
		os.Exit(1)
	}
	err = yaml.Unmarshal(file, &config)
	if err != nil {
		logrus.Error(err.Error())
		os.Exit(1)
	}
	for index := range config.Components {
		setDefaultString(&config.Components[index].Interval, "10s")

		setDefaultInt(&config.Components[index].LatencyThresholds.Slow, 500)
		setDefaultInt(&config.Components[index].LatencyThresholds.Down, 1000)

		setDefaultInt(&config.Components[index].OutageThresholds.Performance, 50)
		setDefaultInt(&config.Components[index].OutageThresholds.Partial, 65)
		setDefaultInt(&config.Components[index].OutageThresholds.Major, 90)

		setDefaultString(&config.Components[index].ThresholdDuration, "5m")

		setDefaultString(&config.Components[index].Http.Method, "GET")
		setDefaultInt(&config.Components[index].Http.Expected.StatusCode, 200)

		setDefaultInt(&config.Components[index].Quassel.Port, 4242)

		setDefaultInt(&config.Components[index].Postgres.Port, 5432)
	}

	storage := NewStorage()

	wg := &sync.WaitGroup{}
	var monitors []*MonitorWrapper
	for index, component := range config.Components {
		logrus.Infof("Starting Monitor #%d: %s", index, component.Name)
		var monitor Monitor
		switch component.Type {
		case "http":
			monitor = NewHttpMonitor(component)
		case "quassel":
			monitor = NewQuasselMonitor(component)
		case "postgres":
			monitor = NewPostgresMonitor(component)
		}
		wrapper := NewMonitorWrapper(&config.Api, &storage, monitor)
		monitors = append(monitors, &wrapper)
		go wrapper.ClockStart(config.Immediate, wg)
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, os.Kill)
	<-signals

	logrus.Warnf("Abort: Waiting monitors to finish")
	for _, mon := range monitors {
		mon.ClockStop()
	}

	wg.Wait()
}
