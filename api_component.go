package main

import "fmt"

import "encoding/json"

const (
	ComponentStatusOperational       = 1
	ComponentStatusPerformanceIssues = 2
	ComponentStatusPartialOutage     = 3
	ComponentStatusMajorOutage       = 4
)

type ApiComponent struct {
	Id          int             `json:"id,omitempty"`
	Name        string          `json:"name,omitempty"`
	Description string          `json:"description,omitempty"`
	Link        string          `json:"link,omitempty"`
	Status      int             `json:"status,omitempty"`
	Order       int             `json:"order,omitempty"`
	GroupId     int             `json:"group_id,omitempty"`
	CreatedAt   string          `json:"created_at,omitempty"`
	UpdatedAt   string          `json:"updated_at,omitempty"`
	DeletedAt   string          `json:"deleted_at,omitempty"`
	StatusName  string          `json:"status_name,omitempty"`
	Tags        json.RawMessage `json:"tags,omitempty"`
}

type ApiComponentBody struct {
	Name        string          `json:"name,omitempty"`
	Description string          `json:"description,omitempty"`
	Link        string          `json:"link,omitempty"`
	Status      int             `json:"status,omitempty"`
	Order       int             `json:"order,omitempty"`
	GroupId     int             `json:"group_id,omitempty"`
	StatusName  string          `json:"status_name,omitempty"`
	Tags        json.RawMessage `json:"tags,omitempty"`
}

type ApiComponentRepo struct {
	ConfigApi
}

func (api ConfigApi) Components() ApiComponentRepo {
	return ApiComponentRepo{
		api,
	}
}

func (repo ApiComponentRepo) Get(id int) (component ApiComponent, err error) {
	_, err = repo.Request(
		&component,
		"GET",
		fmt.Sprintf("/components/%d", id),
		nil,
	)
	return
}

func (repo ApiComponentRepo) Create(body ApiComponentBody) (component ApiComponent, err error) {
	_, err = repo.Request(
		&component,
		"POST",
		"/components",
		body,
	)
	return
}

func (repo ApiComponentRepo) Save(id int, body ApiComponentBody) (component ApiComponent, err error) {
	_, err = repo.Request(
		&component,
		"PUT",
		fmt.Sprintf("/components/%d", id),
		body,
	)
	return
}

func (repo ApiComponentRepo) Delete(id int) (err error) {
	_, err = repo.Request(
		nil,
		"DELETE",
		fmt.Sprintf("/components/%d", id),
		nil,
	)
	return
}
