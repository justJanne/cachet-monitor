package main

import (
	"bufio"
	"crypto/tls"
	"encoding/binary"
	"fmt"
	"net"
	"time"
)

type Connection struct {
	hostname     string
	socket       net.Conn
	tlsSocket    *tls.Conn
	readWriter   *bufio.ReadWriter
	buffer       []byte
	protocolInfo ProtocolInfo
	err          error
}

type ConnectionOptions struct {
	Address string
	Port    int
	Timeout time.Duration
}

func MakeConnection(options ConnectionOptions) Connection {
	socket, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", options.Address, options.Port), options.Timeout)
	if err != nil {
		panic(err.Error())
	}

	return Connection{
		hostname: options.Address,
		socket:   socket,
		readWriter: bufio.NewReadWriter(
			bufio.NewReader(socket),
			bufio.NewWriter(socket),
		),
		buffer: make([]byte, 4),
	}
}

func (c *Connection) Write(data uint32) {
	if c.err == nil {
		binary.BigEndian.PutUint32(c.buffer, data)
		_, c.err = c.readWriter.Write(c.buffer)
	}
}

func (c *Connection) Read(len int) []byte {
	buffer := make([]byte, len)
	if c.err == nil {
		_, c.err = c.readWriter.Read(buffer)
	}
	return buffer
}

func (c *Connection) Flush() {
	if c.err == nil {
		c.err = c.readWriter.Flush()
	}
}

func (c *Connection) WithTLS(verify bool) {
	if c.err == nil {
		config := &tls.Config{
			ServerName:         c.hostname,
			InsecureSkipVerify: !verify,
		}
		c.tlsSocket = tls.Client(c.socket, config)
		c.readWriter = bufio.NewReadWriter(
			bufio.NewReader(c.tlsSocket),
			bufio.NewWriter(c.tlsSocket),
		)
		c.err = c.tlsSocket.Handshake()
	}
}

func (c *Connection) TlsState() *tls.ConnectionState {
	if c.tlsSocket != nil {
		state := c.tlsSocket.ConnectionState()
		return &state
	} else {
		return nil
	}
}

func (c *Connection) ProtocolInfo() ProtocolInfo {
	return c.protocolInfo
}

func (c *Connection) Error() error {
	return c.err
}

func (c *Connection) Close() {
	_ = c.readWriter.Flush()
	_ = c.tlsSocket.Close()
	_ = c.socket.Close()
}
